//
//  ViewController.m
//  assignment1
//
//  Created by Mohammad Jebelli on ۲۰۱۶/۱/۲۰.
//  Copyright © ۲۰۱۶ cs3260. All rights reserved.
//

#import "ViewController.h"

@interface ViewController ()

@end

@implementation ViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    UIScrollView* scrollView = [[UIScrollView alloc]initWithFrame:CGRectMake(0, 0, self.view.frame.size.width, self.view.frame.size.height)];
    //scrollView.backgroundColor = [UIColor blueColor];
    
    [self.view addSubview:scrollView];
    
    
    
    
    [scrollView setContentSize:CGSizeMake(self.view.frame.size.width * 2.2, self.view.frame.size.height * 1.23)];
    
    UIView* redView = [[UIView alloc]initWithFrame:CGRectMake(0, 0, 300, 300)];
    redView.backgroundColor = [UIColor redColor];
    
    [scrollView addSubview:redView];
    
    
    UIView* yellowView = [[UIView alloc]initWithFrame:CGRectMake(300, 0, 300, 300)];
    yellowView.backgroundColor = [UIColor yellowColor];
    
    [scrollView addSubview:yellowView];
    
    
    UIView* blueView = [[UIView alloc]initWithFrame:CGRectMake(600, 0, 300, 300)];
    blueView.backgroundColor = [UIColor blueColor];
    
    [scrollView addSubview:blueView];
    
    
    
    
    UIView* blackView = [[UIView alloc]initWithFrame:CGRectMake(0, 300, 300, 300)];
    blackView.backgroundColor = [UIColor blackColor];
    
    [scrollView addSubview:blackView];
    
    
    UIView* orangeView = [[UIView alloc]initWithFrame:CGRectMake(300, 300, 300, 300)];
    orangeView.backgroundColor = [UIColor orangeColor];
    
    [scrollView addSubview:orangeView];
    
    
    
    
    UIView* purpleView = [[UIView alloc]initWithFrame:CGRectMake(600, 300, 300, 300)];
    purpleView.backgroundColor = [UIColor purpleColor];
    
    [scrollView addSubview:purpleView];
    
    
    
    UIView* greenView = [[UIView alloc]initWithFrame:CGRectMake(0, 600, 300, 300)];
    greenView.backgroundColor = [UIColor greenColor];
    
    [scrollView addSubview:greenView];
    
    
    
    UIView* grayView = [[UIView alloc]initWithFrame:CGRectMake(300, 600, 300, 300)];
    grayView.backgroundColor = [UIColor grayColor];
    
    [scrollView addSubview:grayView];
    
    
    UIView* brownView = [[UIView alloc]initWithFrame:CGRectMake(600, 600, 300, 300)];
    brownView.backgroundColor = [UIColor brownColor];
    
    [scrollView addSubview:brownView];
    
    
    
    // Do any additional setup after loading the view, typically from a nib.
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

@end

