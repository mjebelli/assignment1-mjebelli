//
//  main.m
//  assignment1-mjebelli
//
//  Created by Mohammad Jebelli on ۲۰۱۶/۱/۲۰.
//  Copyright © ۲۰۱۶ cs3260. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "AppDelegate.h"

int main(int argc, char * argv[]) {
    @autoreleasepool {
        return UIApplicationMain(argc, argv, nil, NSStringFromClass([AppDelegate class]));
    }
}
